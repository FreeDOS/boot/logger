; Boot Message Logger Shared Common Macros, Defines and Data

; BSD 3-Clause License
; Copyright (c) 2023, Jerome Shidel

; Redistribution and use in source and binary forms, with or without
; modification, are permitted provided that the following conditions are met:

; 1. Redistributions of source code must retain the above copyright notice, this
;    list of conditions and the following disclaimer.

; 2. Redistributions in binary form must reproduce the above copyright notice,
;    this list of conditions and the following disclaimer in the documentation
;    and/or other materials provided with the distribution.

; 3. Neither the name of the copyright holder nor the names of its
;    contributors may be used to endorse or promote products derived from
;    this software without specific prior written permission.

; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
; DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
; FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
; CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

; NASM 2.15.05, or later

; -----------------------------------------------------------------------------
; optional functionality conditional defines.
; -----------------------------------------------------------------------------

; Include Help text in binaries
%define WITH_HELP

; HTML output support. Adds about 2k to interface portion.
; %define HTML_SUPPORT

; ANSI output support. Adds about 265 bytes to interface portion.
; %define ANSI_SUPPORT

; -----------------------------------------------------------------------------
; experimental and development conditional defines.
; you should probably not change them.
; -----------------------------------------------------------------------------

; Noticed some weirdness when using Direct Capture in 40 column modes under
; VirtualBox. There may also be issues on real hardware or other Virtual
; platforms. So, I'm going to have the driver just use TTL capture in those
; modes. Enable "DIRECT_40_COLUMNS" to use DirectMode mode instead.
; %define DIRECT_40_COLUMNS

; Do not enable, only works under FreeDOS when Driver is loaded in UMB.
; otherwise, system is very unstable if it boots at all.
; %define LOW_MEM_LOG

; -----------------------------------------------------------------------------

%include 'hookamis.inc'

%define MaxXFRSize  80 * 2	; Byte size of the drivers XMS transfer buffer

; -----------------------------------------------------------------------------

%idefine VersionAPI 0x0100	; Required/Supplied Driver API version
				; High/Low = Major/Minor

%idefine Version '1.00'		; Binary's Version

%imacro CopyrightText 0
	%ifndef DEVICE_DRIVER
		db	'Copyright '
	%endif
	db 	'(c)', 0x20
	db 	%substr(__DATE__,1,4), ',', 0x20
	db 	'Jerome Shidel'
	%ifdef DEVICE_DRIVER
		db	', BSD 3-Clause License', 0x0d, 0x0a
	%else
		db 	0x0d, 0x0a
		db	'BSD 3-Clause License', 0x0d, 0x0a, 0x0a
	%endif
%endmacro

%idefine VendorDriverID db 'J.SHIDEL'	; 8 character driver vendor
%idefine DeviceDriverID db 'LOGGERxx'	; 8 character driver name
%idefine ProductName	db 'Message Logger',0	; ASCIIZ, 64 byte max

%idefine DriverIDLength 8

%idefine STDIO_SIZE 512			; Standard size for IO Buffers

; driver status flags (low byte)
%idefine sfEnabled 	00000001b	; bit 0 = logging enabled
%idefine sfModeChange	00000010b	; bit 1 = mode changed, re-config driver
%idefine sfVESAChange	00000100b	; bit 2 = VESA Mode change flag
%idefine sfDirectMode	00001000b	; bit 3 = advanced capture mode active
%idefine sfSupport	00010000b	; bit 4 = card supports advanced mode
%idefine sfInColor	00100000b	; bit 5 = include color attribs in log
					; bit 6 = reserved
%idefine sfLogFull	10000000b	; bit 7 = log full
; driver status flags (high byte) 	  bits 8-15 reserved
%idefine sfLogToLOW	00000000b	; bit 8/9 = Log in LOW memory
%idefine sfLogToEMS	00000001b	; bit 9/9 = Log in EMS memory
%idefine sfLogToUMB	00000010b	; bit 8/9 = Log in UMB memory
%idefine sfLogToXMS	00000011b	; bit 9/9 = Log in XMS memory

; interface status and option flags (low byte) - internal use only
%idefine ofHadOptions	00000001b	; command line options provided
%ifdef WITH_HELP
%idefine ofShowHelp	00000010b	; print help and exit
%endif
%idefine ofStdIn	00000100b	; standard input added to log
%idefine ofBadVersion   00001000b	; incompatible driver version
%idefine ofPreTest	00010000b	; CmdLn Option Pre-Check
%idefine ofPassThru	00100000b	; also send any STDIN to STDOUT
%idefine ofHushMode	01000000b	; do not warn about no driver
; interface status and option flags (high byte)  - internal use only

; interface viewer navigation control flags (low byte)  - internal use only
%idefine vfAtTop	00000001b	; can not scroll up
%idefine vfAtBottom     00000010b	; can not scroll down
%idefine vfAtLeftmost   00000100b	; can not scroll left
%idefine vfAtRightmost  00001000b	; should not scroll right
%idefine vfNoDraw	00010000b	; no draw, calculations only
%idefine vfIsEmptyLog   10000000b	; empty log/no data

; -----------------------------------------------------------------------------

struc TDriverHeader
	.ChainNext: 	resd 1			; pointer to next device driver
	.Attributes:	resw 1			; character device
	.Strategy:	resw 1			; set request block pointer
	.Entry:		resw 1			; driver interrupt call
	.Name:		resb DriverIDLength	; 8 character driver name

	.Dispatch:	resd 1			; far call to driver functions
	.Status:	resw 1			; Device driver Status


	.XMS.Driver:	resd 1			; Pointer to XMS driver
	.XMS.Head:	resd 1			; next buffer write position
	.XMS.Tail:	resd 1			; first buffer read position

	; DO NOT REORDER Size, Count, Max! Driver returns a pointer to these
	; values in the dispatcher status request function.
	.XMS.Size:	resw 1			; Size in KB to allocate
	.XMS.Max:	resd 1			; size of buffer in bytes
	.XMS.Count:	resd 1			; total bytes written to buffer

	.XFR.Count:	resd 1			; byte count { must be even }
	.XFR.SrcHandle:	resw 1			; 0 = conventional memory
	.XFR.SrcAddr:	resd 1			; pointer to source buffer
	.XFR.DstHandle:	resw 1			; XMS handle
	.XFR.DstAddr:	resd 1			; pointer to destination

	.XFR.Buffer:	resb MaxXFRSize 	; TTL XFR Buffer

	.RowSkip:	resw 1			; Number of rows to skip if
						; using DirectMode.

endstruc

; -----------------------------------------------------------------------------

struc TVideoData
	; data copied from BIOS memory area
	.Mode:		resb	1		; current video mode
	.Columns:	resw	1		; screen columns
	.Regen:		resw	1		; regen buffer size
	.Offset:	resw	1		; videop page offset
	.Position:	resw	8		; cursor page positions
	.Cursor:	resw	1		; cursor shape
	.Page:		resb	1		; current video page
	.EndBDA:				; end of copied video BIOS data
	; calculated data
	.Direct:	resb 	1		; direct video supported
	.Rows:		resw 	1		; rows on screen
	.VSeg:		resw 	1		; direct video segment
endstruc

; -----------------------------------------------------------------------------

struc TTransfer
	.Count:		resd 1		; byte count { must be even }
	.SrcHandle:	resw 1		; 0 = conventional memory, or Handle
	.SrcAddr:	resd 1		; pointer to source buffer
	.DstHandle:	resw 1		; XMS handle
	.DstAddr:	resd 1		; pointer to destination
endstruc

; -----------------------------------------------------------------------------

struc TRequest
	.Length:	resb 1		; requested structure length
	.Unit:		resb 1		; unit number for request
	.Function:	resb 1		; requested function
	.Status:	resw 1		; return status
	.Reserved:	resb 8		; reserved
	.Descriptor:	resb 1		; +13, media descriptor BYTE
	.Address:  	resd 1		; +14, transfer address pointer DWORD
					; also, return address byte above
					; program to release after initialized
	.CommandLine:			; +18, init function DWORD
	.Count:		resw 1		; +18
	.Start:		resw 1		; +20
	.Drive:		resb 1		; +22
endstruc

; -----------------------------------------------------------------------------

%idefine IdleCPU	hlt

; -----------------------------------------------------------------------------

%imacro	CheckCompatible 0
	%ifdef DEVICE_DRIVER
		clc
	%else
		cmp	dx, VersionAPI 		; if DX < VersionAPI, CF=1
		jae	%%Done			; if DX >= VersionAPI, CF=0
		PrintMessage BadDriver
		stc				; in case changed by Print
	%%Done:

	%endif
%endmacro

; -----------------------------------------------------------------------------

%imacro PUSHAG 0
	; For 8086, is similar to, but not equivalent to PUSHA!
	; it excludes registers and uses a different order.
	push	ax
	push	bx
	push	cx
	push	dx
	push	si
	push	di
%endmacro

%imacro POPAG 0
	pop	di
	pop	si
	pop	dx
	pop	cx
	pop	bx
	pop	ax
%endmacro

%imacro PUSHALL 0
	pushag
	push	bp
	push	es
	push	ds
%endmacro

%imacro POPALL 0
	push	ds
	push	es
	push	bp
	pushag
%endmacro

; -----------------------------------------------------------------------------

%imacro CMPDD	3
	; Compares a pair registers provided by %1 (High 16-bits) and %2 (Low
	; 16-bits) as a single 32-bit value to a memory location %3.
	cmp 	%1, [%3+2]
	jne	%%Done
	cmp	%2, [%3]
%%Done:
%endmacro

; -----------------------------------------------------------------------------

%imacro IISP 1-2
	; Creates an IBM Interrupt Sharing Protocol header

			%if %0 = 1
				%warning RETF instruction inserted before IISP %1
			%1_FAR_RETURN:
				retf
			%endif
%1:
.EntryPoint:		jmp	strict short .EndOfHeaderIISP
.NextHandler:		dd	0		; Next Handler in the chain
			dw	0x424b		; IISP Signature
			db	0x00		; EOI Flag
			%if %0 = 1
				jmp	strict short %1_FAR_RETURN
			%else
				jmp	strict short %2
			%endif
			times 7 db 0		; 7 Reserved bytes
.EndOfHeaderIISP:
.InterruptHandler:
%endmacro

; -----------------------------------------------------------------------------

%imacro ByteAsChar 1-*
	%rep %0
		push	ax
		%ifnidni %1, dl
			push	dx
			mov	dl, %1
		%endif
		mov	ah, 0x02
		int	0x21
		%ifnidni %1, dl
			pop	dx
		%endif
		pop	ax
	%rotate 1
	%endrep
%endmacro

; -----------------------------------------------------------------------------

%macro INTERNAL_NibbleAsHex 0
PROC_NibbleAsHex:
	; low 4 bits of al
	push	ax
	push	dx
	and	al, 0x0f
	mov	dl, 0x30
	cmp	al, 0x09
	jbe	%%NotChar
	add	al, 0x07
%%NotChar:
	add	dl, al
	mov	ah, 0x02
	int	0x21
	pop	dx
	pop	ax
	ret
%endmacro

%imacro NibbleAsHex 1
	%define NEED_NibbleAsHex
	%ifnidni %1, al
		push	ax
		mov	al, %1
	%endif
	call	PROC_NibbleAsHex
	%ifnidni %1, al
		pop	ax
	%endif

%endmacro

; -----------------------------------------------------------------------------

%macro INTERNAL_ByteAsBin 0
PROC_ByteAsBin:
	push		cx
	push		ax
	mov		cx, 0x08
%%Loop:
	test		al, 0x80
	jz		%%Zero
	ByteAsChar	'1'
	jmp		%%Next
%%Zero:
	ByteAsChar	'0'
%%Next:
	shl		al, 1
	loop		%%Loop
	pop		ax
	pop		cx
	ret
%endmacro

%imacro ByteAsBin 1
	%define NEED_ByteAsBin
	%ifnidni %1, al
		push	ax
		mov	al, %1
	%endif
	call	PROC_ByteAsBin
	%ifnidni %1, al
		pop	ax
	%endif
%endmacro

; -----------------------------------------------------------------------------

%macro INTERNAL_ByteAsHex 0
PROC_ByteAsHex:
	push		cx
	push		ax
	mov		cl, 0x04
	shr		ax, cl
	NibbleAsHex 	al
	pop		ax
	pop		cx
	NibbleAsHex 	al
	ret
%endmacro

%imacro ByteAsHex 1
	%define NEED_ByteAsHex
	%ifnidni %1, al
		push	ax
		mov	al, %1
	%endif
	call	PROC_ByteAsHex
	%ifnidni %1, al
		pop	ax
	%endif
%endmacro

; -----------------------------------------------------------------------------

%macro INTERNAL_WordAsHex 0
PROC_WordAsHex:
	xchg		al, ah
	ByteAsHex	al
	xchg		al, ah
	ByteAsHex	al
	ret
%endmacro

%imacro WordAsHex 1-*
	%rep %0
		%define NEED_WordAsHex
		%ifnidni %1, ax
			push	ax
			mov	ax, %1
		%endif
		call	PROC_WordAsHex
		%ifnidni %1, ax
			pop	ax
		%endif
	%rotate 1
	%endrep
%endmacro

; -----------------------------------------------------------------------------

%macro INTERNAL_WordAsUInt 0
PROC_WordAsUInt:
	push		ax
	push		bx
	push		cx
	push		dx
	mov		bx, 0x000a
	mov		cx, 0x0001
%%Loop:
	cmp		ax, bx
	jae		%%OverNine
	push		ax
%%UnderTen:
	pop		ax
	add		al, 0x30
	ByteAsChar	al
	loop        	%%UnderTen
	jmp		%%Done
%%OverNine:
	inc		cx
	xor		dx, dx
	div		bx
	push		dx
	jmp		%%Loop
%%Done:
	pop		dx
	pop		cx
	pop		bx
	pop		ax
	ret
%endmacro

%imacro WordAsUInt 1
	%define NEED_WordAsUInt
	%ifnidni %1, ax
		push	ax
		mov	ax, %1
	%endif
	call	PROC_WordAsUInt
	%ifnidni %1, ax
		pop	ax
	%endif
%endmacro

%imacro ByteAsUInt 1
	push		ax
	mov		al, %1
	xor		ah, ah
	WordAsUInt 	ax
	pop		ax
%endmacro

; -----------------------------------------------------------------------------

%macro INTERNAL_ParseOptions 0
PROC_ParseOptions:
.MainLoop:
	mov	cx, di
.GetOptLoop:
	mov	dl, [es:di]
	; check for separator
	cmp	dl, 0x20
	jbe	.GetOptDone	; anything that is a space or lower
	cmp	dl, 0x3b
	je	.GetOptDone	; semi-colon
	cmp	dl, 0x2c
	je	.GetOptDone	; comma
	inc	di
	jmp	.GetOptLoop
.GetOptDone:
	cmp	cx, di
	jne	.SearchOption
.GetOptNext:
	; if it was a separator, continue processing
	cmp	[es:di], byte 0x20 ; space
	je	.NotDone
	cmp	[es:di], byte 0x2c ; comma
	je	.NotDone
	cmp	[es:di], byte 0x3b ; semi-colon
	je	.NotDone
	ret
.NotDone:
	inc	di
	jmp	.MainLoop

.SearchOption:
	push	si
	; cld
.NextOpt:
	mov	bx, cx  ; start of current option text
.SearchLoop:
	lodsw
	test	ax, ax
	jz	.NotFound
	mov	dx, ax	; option function pointer
	jmp	.CompareLoop
.NotFound:
	lodsw
	mov	dx, ax  ; Catch all function
	jmp	.Matched
.CompareLoop:
	mov	ah, [es:bx]
	inc	bx
	cmp	ah, 0x61
	jb	.NotLowerCase
	cmp	ah, 0x7a
	ja	.NotLowerCase
	sub	ah, 0x20
.NotLowerCase:
	lodsb
	cmp	al, ah
	jne	.MissMatch
	cmp	bx, di
	jne	.CompareLoop
	mov	al, [si]
	test	al, al
	jnz	.MissMatch
.Matched:
	mov	si, cx
	call	dx
	jmp	.SearchDone
.MissMatch:
	test	al, al
	jz	.NextOpt
	lodsb
	jmp	.MissMatch
.SearchDone:
	pop	si
	jmp	.GetOptNext

%endmacro

%macro ParseOptions 2
	%define NEED_ParseOptions
	mov	si, %1
	%ifnidni %2, di
		mov	di, %2
	%endif
	call	PROC_ParseOptions
%endmacro

; -----------------------------------------------------------------------------

%imacro PrintOptionText 0
	; cld
%%Loop:
	cmp		si, di
	je		%%Done
	es lodsb
	ByteAsChar	al
	jmp		%%Loop
%%Done:
%endmacro

; -----------------------------------------------------------------------------

%imacro OptionAsWord 0
	push	    	si
	xor         	ax, ax
	mov         	bl, [es:si]
	cmp		bl, '-'
	je		%%Negative
%%DecLoop:
	cmp	   	si, di
	jz	   	%%Success
	call		%%GetChar
	cmp        	bl, 'X'
	je         	%%AsHex
	sub        	bl, 0x30
	cmp	    	bl, 0x09
	ja	    	%%AsHex
	mov         	cx, 10
	mul         	cx
	xor         	bh, bh
	add         	ax, bx
	jc	    	%%Overflow
	test	    	dx, dx
	jnz	    	%%Overflow
	jmp        	%%DecLoop

%%GetChar:
	mov         	bl, [es:si]
	inc        	si
	cmp		bl, 0x61
	jb		%%NotLower
	cmp		bl, 0x7a
	ja		%%NotLower
	sub		bl, 0x20
%%NotLower:
	ret

%%AsHex:
	xor	    	ax, ax
	pop	    	si
	push        	si

%%HexLoop:
	cmp	   	si, di
	jz	    	%%Success
	call		%%GetChar
	cmp	    	bl, 'X'
	jne	    	%%NotAnX
	test	    	ax, ax
	jz	    	%%HexLoop
	jmp	    	%%Invalid
%%NotAnX:
	sub	    	bl, 0x30
	cmp         	bl, 0x0a
	jb	    	%%Shifting
	cmp	    	bl, 0x10
	jbe	    	%%Invalid
	cmp	    	bl, 0x16
	ja	    	%%Invalid
	sub	    	bl, 0x07
%%Shifting:
	clc
	mov	    	cx, 4
%%ShiftBits:
	rcl	    	ax, 1
	jc	    	%%Overflow
	loop	    	%%ShiftBits
	or	    	al, bl
	jmp	    	%%HexLoop

%%Invalid:
%%Overflow:
%%Negative:
%%Failure:
	stc
	jmp		%%Done
%%Success:
	clc
%%Done:
	pop		si
%endmacro

; -----------------------------------------------------------------------------

%imacro PrintMessage 0-1
	%if %0 = 1
		%ifnidni %1, dx
			mov	dx, %1
		%endif
	%endif
	mov	ah, 0x09
	int	0x21
%endmacro

; -----------------------------------------------------------------------------

%imacro PrintUsage 0

	; incomming registers:
	; CX is status
	; ES:DI-> size info record

	PrintMessage	%%Buffer
	mov		ax, [es:di+6]  ; get byte written count as dx:ax
	mov		dx, [es:di+8]
	mov		bx, ax
	or		bx, dx
	test		bx, bx
	jz		%%Empty
	test 		cl, byte sfLogFull
	jnz		%%Full
	mov		cx, [es:di+2]	; get log capacity in bytes as bc:cx
	mov		bx, [es:di+4]
%%Percent:
	; calculate and display percentage
%%Reduce:
	; reduce both count and max to a single word
	push		di
	xor		di, di
	call		%%ShiftSwap
	call		%%ShiftSwap
	test		di, di
	pop		di
	jz		%%Reduced
	jmp		%%Reduce

%%ShiftSwap:
	; shift dx:ax right
	clc
	xchg		ax, dx
	rcr		ax, 1
	xchg		ax, dx
	rcr		ax, 1
	or		di, dx
	push		ax
	push		dx
	push		cx
	mov		cx, 100
	mul		cx
	or		di, dx
	pop		cx
	pop		dx
	pop		ax

	; swap dx:ax with bx:cx
	xchg		ax, cx
	xchg		dx, bx
	ret

%%Reduced:
	mov		bx, 100
	mul		bx
	div		cx
	WordAsUInt	ax
	PrintMessage	%%IsPercent
	jmp		%%And
%%Empty:
	PrintMessage	%%IsEmpty
	jmp		%%And
%%Full:
	PrintMessage	%%IsFull
%%And:
	PrintMessage	%%Logging
	cmp		[DRIVER_ENABLED], byte 1
	je		%%On
	PrintMessage	%%IsOff
	PrintMessage	%%CRLF
	jmp		%%Done
%%On:
	PrintMessage	%%IsOn
	jmp		%%Done

%%Buffer:	db 'The buffer is $'
%%IsEmpty:	db 'empty$'
%%IsPercent:    db '% '
%%IsFull:	db 'full$'
%%Logging:	db ' and logging is $'
%%IsOff:	db 'disabled.$'
%%IsOn:		db 'active.'
%%CRLF:		db 0x0d,0x0a,'$'

%%Done:
%endmacro

; -----------------------------------------------------------------------------

%imacro PrintStatus 0

%ifdef DEVICE_DRIVER
	WordAsUInt	[TDriverHeader.XMS.Size]
%else
	push		es
	mov		al, 0x10
	call far 	[DRIVER_CALL]
	mov		es, dx
	WordAsUInt	[es:di]
%endif

	PrintMessage	%%KB

	; each memory region string is 4-bytes long, LOW, EMS, UMB, XMS
	xor		dh, dh
%ifdef DEVICE_DRIVER
	mov		dl, [TDriverHeader.Status+1]
%else
	mov		dl, ch
%endif
	and		dl, 00000011b
	add		dx, dx
	add		dx, dx
	add		dx, %%REGION
	PrintMessage	dx

%%Allocated:
	PrintMessage	%%MEM
%ifdef DEVICE_DRIVER
	test		[TDriverHeader.Status], byte sfInColor
%else
	test		cl, sfInColor
%endif
	jnz		%%InColor
	mov		dx, %%MONO
	jmp		%%Logging
%%InColor:
	PrintMessage	%%COLOR
	mov		dx, %%LOG
%%Logging:
	PrintMessage	dx

%ifndef DEVICE_DRIVER
	PrintUsage
	pop		es
%endif
	jmp	%%Done

%%KB:	db	'KB of $'
%%MEM:	db	' memory allocated for $'

%%REGION:
	db	'LOW$'
	db	'EMS$'
	db	'UMB$'
	db	'XMS$'

%%COLOR:db	'color$'
%%MONO: db	'monochrome'
%%LOG:	db	' logging.',0x0d,0x0a,'$'

%%Done:
%endmacro

; -----------------------------------------------------------------------------

%macro INTERNAL_StdIn 0
PROC_StdIn:
	push		bx
	push		ax
	mov		ax, [StdIn_Data]
	test		ax, ax
	jnz		%%InputWaiting
	mov		[StdIn_Data+2], word StdIn_Buffer

	; read a block of data from Standard Input
	push		dx
	push		cx
	mov		ah, 0x3f	; read from file function
	xor		bx, bx		; file handle
	mov		cx, STDIO_SIZE	; maximum number of bytes to read
	mov		dx, StdIn_Buffer; DS:DX->buffer for data
	int		0x21
	pop		cx
	pop		dx
	jc		%%Done		; Was there an error?
	; AX=Number of bytes read. If AX=0 then it was at EOF before the call
	test		ax, ax		; At end of File?
	jz		%%NoInput
	mov		[StdIn_Data], ax ; Save Count
%%InputWaiting:
	mov		bx, [StdIn_Data+2]
	mov		al, [bx]
	inc		bx
	mov		[StdIn_Data+2], bx
	dec		word [StdIn_Data]
	clc
	jmp		%%Done
%%NoInput:
	stc
%%Done:
	mov		bl, al
	pop		ax
	mov		al, bl
	pop		bx
	ret
	; CY set, no input al=0
	; CY clear, al=character
%endmacro

%imacro StdIn 1
	%define NEED_StdIn
	%ifnidni %1, al
		%warning StdIn requires AL as a parameter.
		call	PROC_StdIn
		mov	%1, al
	%else
		call	PROC_StdIn
	%endif
%endmacro

; -----------------------------------------------------------------------------

%macro INTERNAL_StdOut 0
PROC_StdOut:
	pushf
	push		bx
	mov		bx, [StdOut_Data]
	mov		[bx+StdOut_Buffer], al
	inc		bx
	mov		[StdOut_Data], bx
	cmp		bx, STDIO_SIZE
	jne		%%NoFlush
	call		.Flush
%%NoFlush:
	pop		bx
	popf
	ret
.Flush:
	push		ax
	push		bx
	push		cx
	push		dx
	mov		cx, [StdOut_Data]	; Number of bytes
	test		cx, cx
	jz		%%NoWrite
	mov		ah, 0x40
	mov		bx, 0x0001		; StdOut Handle
	mov		dx, StdOut_Buffer	; ds:dx -> buffer
	int		0x21
	mov		[StdOut_Data], word 0
%%NoWrite:
	pop		dx
	pop		cx
	pop		bx
	pop		ax
	ret
%endmacro

%macro INTERNAL_StdOutDL 0
PROC_StdOutDL:
	push	ax
	mov	al, dl
	call	PROC_StdOut
	pop	ax
	ret
%endmacro

%macro INTERNAL_StdOutDX 0
PROC_StdOutDX:
	pushf
	push	ax
	push	si
	mov	si, dx
	cld
.Again:
	lodsb
	cmp	al, '$'
	je	.Done
	call	PROC_StdOut	; al
	jmp	.Again
.Done:
	pop	si
	pop	ax
	popf
	ret
%endmacro

%imacro StdOut 1
	%define NEED_StdOut
	%ifstr %1
		%if %strlen(%1) = 1
			push	ax
			mov	al, %1
			call	PROC_StdOut
			pop	ax
		%else
			%fatal StdOut has no string output
		%endif
	%elifidni %1, al
		call	PROC_StdOut
	%elifidni %1, dl
		%define NEED_StdOutDL
		call	PROC_StdOutDL
	%elifidni %1, dx
		%define NEED_StdOutDX
		call	PROC_StdOutDX
	%else
		%define NEED_StdOutDX
		push	dx
		mov	dx, %1
		call	PROC_StdOutDX
		pop	dx
	%endif

%endmacro

%macro INTERNAL_StdOutWordAsUInt 0
PROC_StdOutWordAsUInt:
	push		ax
	push		bx
	push		cx
	push		dx
	mov		bx, 0x000a
	mov		cx, 0x0001
%%Loop:
	cmp		ax, bx
	jae		%%OverNine
	push		ax
%%UnderTen:
	pop		ax
	add		al, 0x30
	call		PROC_StdOut	; al
	loop        	%%UnderTen
	jmp		%%Done
%%OverNine:
	inc		cx
	xor		dx, dx
	div		bx
	push		dx
	jmp		%%Loop
%%Done:
	pop		dx
	pop		cx
	pop		bx
	pop		ax
	ret
%endmacro

%imacro StdOutUInt 1
	%define NEED_StdOut
	%define NEED_StdOutWordAsUInt
	%ifnidni %1, ax
		push	ax
		mov	ax, %1
	%endif
	call	PROC_StdOutWordAsUInt
	%ifnidni %1, ax
		pop	ax
	%endif
%endmacro

%macro INTERNAL_StdOutNibbleAsHex 0
PROC_StdOutNibbleAsHex:
	; low 4 bits of al
	push	ax
	push	dx
	and	al, 0x0f
	mov	dl, 0x30
	cmp	al, 0x09
	jbe	%%NotChar
	add	al, 0x07
%%NotChar:
	add	al, dl
	call	PROC_StdOut
	pop	dx
	pop	ax
	ret
%endmacro

%macro INTERNAL_StdOutByteAsHex 0
PROC_StdOutByteAsHex:
	push		cx
	push		ax
	mov		cl, 0x04
	shr		ax, cl
	call		PROC_StdOutNibbleAsHex
	pop		ax
	pop		cx
	call		PROC_StdOutNibbleAsHex
	ret
%endmacro

%macro INTERNAL_StdOutWordAsHex 0
PROC_StdOutWordAsHex:
	xchg		al, ah
	call		PROC_StdOutByteAsHex
	xchg		al, ah
	call		PROC_StdOutByteAsHex
	ret
%endmacro

%imacro StdOutHex 1-*
	%define NEED_StdOutNibbleAsHex
	%define NEED_StdOutByteAsHex
	%define NEED_StdOutWordAsHex
	%rep %0
		%ifnidni %1, ax
			push	ax
			mov	ax, %1
		%endif
		call	PROC_StdOutWordAsHex
		%ifnidni %1, ax
			pop	ax
		%endif
	%rotate 1
	%endrep
%endmacro


%imacro StdOutFlush 0
	%define NEED_StdOut
	call	PROC_StdOut.Flush
%endmacro

; -----------------------------------------------------------------------------

%macro INTERNAL_VideoSettings 0
PROC_VideoSettings:
	; copy video settings for later
	push		di
	push		si
	push		es
	push		ds

	push		cs
	pop		es
	mov		ax, 0x0040
	mov		ds, ax
	mov		si, 0x0049
	mov		di, VideoData
	mov		cx, TVideoData.EndBDA
	cld
	rep		movsb
	; check that we are in a compatible text mode to save screen
	mov		bx, 0xb000 	; mono segment
	mov		al, [0x0049]	; video mode
	cmp		al, 0x07
	je		.ModeOk
	mov		bx, 0xb800 	; color segment
	cmp		al, 0x03
	jbe		.ModeOk
	; possibly support other modes like 132x50
	jmp		.ModeBad
.ModeOk:
	mov		ax, [0x004c]	; regen size
	test		ax, ax
	jz		.ModeBad
	cmp		ax, 0x4000
	ja		.ModeBad
	mov		[es:di], byte 1	; TVideoData.Direct
	clc
	jmp		.SetRows
.ModeBad:
	mov		[es:di], byte 0	; TVideoData.Direct
	stc

.SetRows:
	pushf
	inc		di
	mov		ax, [0x004c]	; regen size
	mov		cx, [0x004a] 	; columns
	push		ax
	push		cx
	xor		dh, dh
	mov		dl, [0x0084]	; rows
	test		dx, dx
	jz		.CalcRows
	inc		dx
	mov		cx, dx
	jmp		.DoneRows
.CalcRows:
;	xor		dx, dx
	shr		ax, 1
	div		cx	     	; ax=calculated rows
	mov		cx, ax
.DoneRows:
	pop		ax		; was cx, columns
	pop		dx		; was ax, regen
	popf
.Done:
	mov		[es:di], cx	; TVideoData.Rows
	mov		[es:di+2], bx	; TVideoData.VSeg
	pop		ds
	pop		es
	pop		si
	pop		di
	; VideoData always populated, VideoDirect = 1 compatible, 0 incompatible
	; CY set if incompatible to direct video mode or copy
	; CY clear if compatible, bx=video segment, dx=regen words, ax=columns,
	; cx=rows
	ret

%endmacro

%imacro VideoSettings 0
	%define NEED_VideoSettings
	call	PROC_VideoSettings
%endmacro

; -----------------------------------------------------------------------------


%imacro ConditionalCode 1
	%ifdef NEED_%1
		%ifndef AVAIL_%1
			INTERNAL_%1
			%define AVAIL_%1
		%endif
	%endif
%endmacro

%imacro CommonCode 0
	ConditionalCode	ParseOptions
	ConditionalCode VideoSettings
	ConditionalCode StdIn
	ConditionalCode StdOut
	ConditionalCode StdOutDL
	ConditionalCode StdOutDX
	ConditionalCode StdOutWordAsUInt
	ConditionalCode StdOutFlush
	ConditionalCode	StdOutNibbleAsHex
	ConditionalCode	StdOutByteAsHex
	ConditionalCode	StdOutWordAsHex

	ConditionalCode	WordAsUInt
	ConditionalCode	WordAsHex
	ConditionalCode	ByteAsHex
	ConditionalCode	ByteAsBin
	ConditionalCode	NibbleAsHex
%endmacro