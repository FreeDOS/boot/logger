LOG2HTML.COM [ HELP ]

	HELP, ?		Display this help text.

Output LOGGER's log in HTML format to the Standard Output device. In order to
save the log to a HTML file, console output must be redirected. For example:

LOG2HTML >MYLOG.HTM
