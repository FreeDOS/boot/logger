; BSD 3-Clause License
; Copyright (c) 2023, Jerome Shidel

; Redistribution and use in source and binary forms, with or without
; modification, are permitted provided that the following conditions are met:

; 1. Redistributions of source code must retain the above copyright notice, this
;    list of conditions and the following disclaimer.

; 2. Redistributions in binary form must reproduce the above copyright notice,
;    this list of conditions and the following disclaimer in the documentation
;    and/or other materials provided with the distribution.

; 3. Neither the name of the copyright holder nor the names of its
;    contributors may be used to endorse or promote products derived from
;    this software without specific prior written permission.

; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
; DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
; FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
; CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

; NASM 2.15.05, or later

; -----------------------------------------------------------------------------

PrintHTML:
%ifdef AVAIL_PrepareToRead
	mov		[LastColor], byte 0x07
	mov		[BackSpan], byte 0x00
	mov		[ForeSpan], byte 0x00
	mov		[HTMLWidth], word 0x00
%endif

	StdOut		HTML.HEADER

	; Fetch position of first character to print from the log

%ifdef AVAIL_PrepareToRead
	call		PrepareToRead
%else
	mov		al, 0x16	; Read Log
	mov		bl, 0x05	; subfunction, Get first whole line
	call far	[DRIVER_CALL]	; bypass INT 0x2d multiplexer
					; DX:CX is position data
%endif

	cmp		dx, -1		; DX=0xffff (-1) if log is empty
	je		.Empty

.PrintLoop:

	mov		al, 0x16	; Read Log
	mov		bl, 0x01	; subfunction, Get Character/Color and
					; update DX for next position
	call far	[DRIVER_CALL]	; bypass INT 0x2d multiplexer
					; DX:CX is new position data or -1 (end)
					; BL is character
					; BH is color (if log is mono, BH=0x07)

	; save character position data
	push 		cx
	push		dx

	mov		dx, bx

	inc		word [HTMLWidth]
	cmp		dl, 0x0d
	jne		.NotCR
	mov		dh, 0x07
	test		[HTMLFixed], byte 1
	jnz		.NotCR
	call		.HTMLFix
.NotCR:
	cmp		dl, 0x0a
	jne		.NotLF
	mov		dh, 0x07
.NotLF:

	cmp		dh, [LastColor]
	je		.ColorIsSet
	mov		[LastColor], dh

	call		.ColorSpanOff

	push		bx
	push		dx
	and		dh, 0xf0
	cmp		dh, 0x00
	je		.BackIsDefault
	mov		al, dh
	push		cx
	mov		cl, 4
	shr		al, cl
	pop		cx
	mov		bl, 'b'
	mov		[BackSpan], byte 1
	call		.ColorSpanOn
.BackIsDefault:
	pop		dx
	push		dx
	and		dh, 0x0f
	cmp		dh, 0x07
	je		.ForeIsDefault
	mov		al, dh
	mov		bl, 'c'
	mov		[ForeSpan], byte 1
	call		.ColorSpanOn
.ForeIsDefault:
	pop		dx
	pop		bx

.ColorIsSet:
	call		.PrintChar

.PrintedChar:
	; restore character position data
	pop		dx
	pop		cx

	cmp		dx, -1
	jne		.PrintLoop

	call		.ColorSpanOff
	jmp		.Done

.ColorSpanOff:
	; turn off any prior active color change spans
	cmp 		[ForeSpan], byte 0
	jz		.ForeWasDefault
	mov		[ForeSpan], byte 0
	StdOut		HTML.ENDSPAN
.ForeWasDefault:
	cmp 		[BackSpan], byte 0
	je		.BackWasDefault
	mov 		[BackSpan], byte 0
	StdOut		HTML.ENDSPAN
.BackWasDefault:
	ret

.ColorSpanOn:
	push		ax
	StdOut		HTML.STARTSPAN
	mov		al, bl
	StdOut		al
	pop		ax
	xor		ah, ah
	StdOutUInt	ax
	StdOut		'"'
	StdOut		'>'
	ret

.PrintChar:
	push		ax
	push		dx
	push		si

	mov		al, dl
	mov		si, HTML.TABLE_A
	cmp		dl, 0x1f
	jbe		.Remap_A
	cmp		dl, 0x7f
	jae		.Remap_B
	jmp		.Remap_C

.Remap_B:
	mov		si, HTML.TABLE_B
	sub		al, 0x7f
.Remap_A:
	xor		ah, ah
	add		ax, ax
	add		si, ax
	mov		ax, [si]
	test		ax, ax
	jz		.NoRemap
	push		ax
	StdOut		HTML.PREFIX
	pop		ax
	StdOutHex	ax
	mov		al, ';'
	StdOut		al
	jmp		.DoneReplace

.Remap_C:
	mov		si, HTML.TABLE_C
.Lookup_C:
	lodsb
	cmp		al, dl
	je		.Match_C
	test		al, al
	jz		.NoRemap
.Next_C:
	lodsb
	test		al, al
	jz		.Lookup_C
	jmp		.Next_C

.Match_C:
	lodsb
	test		al, al
	jz		.DoneReplace
	StdOut		al
	jmp		.Match_C

.HTMLFix:
	cmp		[HTMLWidth], word 80
	jae		.HTMLFixDone
	push		dx
	push		cx
	StdOut	 	HTML.FIXPRE
	mov		cx, 80
	sub		cx, [HTMLWidth]
	mov		dl, 0x20
.HTMLPadding:
	StdOut		dl
	loop		.HTMLPadding
	StdOut	 	HTML.FIXPOST
	pop		cx
	pop		dx
.HTMLFixDone:
	mov		[HTMLFixed], byte 1
	ret

.NoRemap:
	; Not Remapped
	StdOut		dl

.DoneReplace:
	pop		si
	pop		dx
	pop		ax
	ret

.Empty:
	StdOut 		LogEmpty
.Done:
.Footer:
	StdOut		HTML.FOOTER
	StdOutFlush
	ret

BackSpan:	db 0
ForeSpan:	db 0
HTMLFixed:	db 0
HTMLWidth:	dw 0

HTML.HEADER:
	db '<html>'
	db '<style>'
	db 'body{background:#ccc;}'
	db '.outer{display:inline-block;'
	db 'white-space:pre;font-family: Menlo,monospace;'
	db 'background:#222;padding:0.5pc;border-radius:0.75pc;}'
	db '.inner{background: black;color: gray;width:auto;}'
	db '.c0{color:#000;}'
	db '.c1{color:#00a;}'
	db '.c2{color:#0a0;}'
	db '.c3{color:#0aa;}'
	db '.c4{color:#a00;}'
	db '.c5{color:#a0a;}'
	db '.c6{color:#a50;}'
	db '.c7{color:#aaa;}'
	db '.c8{color:#555;}'
	db '.c9{color:#55f;}'
	db '.c10{color:#5f5;}'
	db '.c11{color:#5ff;}'
	db '.c12{color:#f55;}'
	db '.c13{color:#f5f;}'
	db '.c14{color:#ff5;}'
	db '.c15{color:#fff;}'
	db '.b0{background:#000;}'
	db '.b1{background:#00a;}'
	db '.b2{background:#0a0;}'
	db '.b3{background:#0aa;}'
	db '.b4{background:#a00;}'
	db '.b5{background:#a0a;}'
	db '.b6{background:#a50;}'
	db '.b7{background:#aaa;}'
	db '.b8{background:#000;}'
	db '.b9{background:#00a;}'
	db '.b10{background:#0a0;}'
	db '.b11{background:#0aa;}'
	db '.b12{background:#a00;}'
	db '.b13{background:#a0a;}'
	db '.b14{background:#a50;}'
	db '.b15{background:#aaa;}'
	db '</style>',0x0d,0x0a
	db '<body>'
	db '<div class="outer"><div class="inner">$'
HTML.FOOTER:
	db '</div></div></body></html>',0x0d,0x0a,'$'
HTML.ENDSPAN:
	db '</span>$'
HTML.STARTSPAN:
	db '<span class="$'
HTML.FIXPRE:
	db '<!-- pad 80 col -->$'
HTML.FIXPOST:
	db '<!-- end pad -->$'

HTML.PREFIX:
	db '&#x$'


%imacro htmldata 2
	; db %1
	dw %2
%endmacro

HTML.TABLE_A:
	htmldata 0x00,0x00a0	; 0x2400
	htmldata 0x01,0x263a
	htmldata 0x02,0x263b
	htmldata 0x03,0x2665
	htmldata 0x04,0x2666
	htmldata 0x05,0x2663
	htmldata 0x06,0x2660
	htmldata 0x07,0x2022
	htmldata 0x08,0x2424	; 0x2fd8,0x2408
	htmldata 0x09,0x25cb
 	htmldata 0x0a,0		; 0x25d9
	htmldata 0x0b,0x2642
	htmldata 0x0c,0x2640
 	htmldata 0x0d,0		; 0x266a
	htmldata 0x0e,0x266b
	htmldata 0x0f,0x263c

	htmldata 0x10,0x25b8	; 0x25ba,0x2023
	htmldata 0x11,0x25c2	; 0x25c4,0x25c0
	htmldata 0x12,0x2195
	htmldata 0x13,0x203c
	htmldata 0x14,0x00b6
	htmldata 0x15,0x00a7
	htmldata 0x16,0x25ac
	htmldata 0x17,0x21a8
	htmldata 0x18,0x2191
	htmldata 0x19,0x2193
	htmldata 0x1a,0x2192
	htmldata 0x1b,0x2190
	htmldata 0x1c,0x221f
	htmldata 0x1d,0x2194
	htmldata 0x1e,0x25b4	; 0x25b2
	htmldata 0x1f,0x25be	; 0x25bc

HTML.TABLE_B:

	htmldata 0x7f,0x2302
	htmldata 0x80,0x00c7
	htmldata 0x81,0x00fc
	htmldata 0x82,0x00e9
	htmldata 0x83,0x00e2
	htmldata 0x84,0x00e4
	htmldata 0x85,0x00e0
	htmldata 0x86,0x00e5
	htmldata 0x87,0x00e7
	htmldata 0x88,0x00ea
	htmldata 0x89,0x00eb
	htmldata 0x8a,0x00e8
	htmldata 0x8b,0x00ef
	htmldata 0x8c,0x00ee
	htmldata 0x8d,0x00ec
	htmldata 0x8e,0x00c4
	htmldata 0x8f,0x00c5

	htmldata 0x90,0x00c9
	htmldata 0x91,0x00e6
	htmldata 0x92,0x00c6
	htmldata 0x93,0x00f4
	htmldata 0x94,0x00f6
	htmldata 0x95,0x00f2
	htmldata 0x96,0x00fb
	htmldata 0x97,0x00f9
	htmldata 0x98,0x00ff
	htmldata 0x99,0x00d6
	htmldata 0x9a,0x00dc
	htmldata 0x9b,0x00a2
	htmldata 0x9c,0x00a3
	htmldata 0x9d,0x00a5
	htmldata 0x9e,0x20a7
	htmldata 0x9f,0x0192

	htmldata 0xa0,0x00e1
	htmldata 0xa1,0x00ed
	htmldata 0xa2,0x00f3
	htmldata 0xa3,0x00fa
	htmldata 0xa4,0x00f1
	htmldata 0xa5,0x00d1
	htmldata 0xa6,0x00aa
	htmldata 0xa7,0x00ba
	htmldata 0xa8,0x00bf
	htmldata 0xa9,0x231c	; 0x2310
	htmldata 0xaa,0x231d	; 0x00aa
	htmldata 0xab,0x00bd
	htmldata 0xac,0x00bc
	htmldata 0xad,0x00a1
	htmldata 0xae,0x00ab
	htmldata 0xaf,0x00bb

	htmldata 0xb0,0x2591
	htmldata 0xb1,0x2592
	htmldata 0xb2,0x2593
	htmldata 0xb3,0x2502
	htmldata 0xb4,0x2524
	htmldata 0xb5,0x2561
	htmldata 0xb6,0x2562
	htmldata 0xb7,0x2556
	htmldata 0xb8,0x2555
	htmldata 0xb9,0x2563
	htmldata 0xba,0x2551
	htmldata 0xbb,0x2557
	htmldata 0xbc,0x255d
	htmldata 0xbd,0x255c
	htmldata 0xbe,0x255b
	htmldata 0xbf,0x2510

	htmldata 0xc0,0x2514
	htmldata 0xc1,0x2534
	htmldata 0xc2,0x252c
	htmldata 0xc3,0x251c
	htmldata 0xc4,0x2500
	htmldata 0xc5,0x253c
	htmldata 0xc6,0x255e
	htmldata 0xc7,0x255f
	htmldata 0xc8,0x255a
	htmldata 0xc9,0x2554
	htmldata 0xca,0x2569
	htmldata 0xcb,0x2566
	htmldata 0xcc,0x2560
	htmldata 0xcd,0x2550
	htmldata 0xce,0x256c
	htmldata 0xcf,0x2567

	htmldata 0xd0,0x2568
	htmldata 0xd1,0x2564
	htmldata 0xd2,0x2565
	htmldata 0xd3,0x2559
	htmldata 0xd4,0x2558
	htmldata 0xd5,0x2552
	htmldata 0xd6,0x2553
	htmldata 0xd7,0x256b
	htmldata 0xd8,0x256a
	htmldata 0xd9,0x2518
	htmldata 0xda,0x250c
	htmldata 0xdb,0x2588
	htmldata 0xdc,0x2584
	htmldata 0xdd,0x258c
	htmldata 0xde,0x2590
	htmldata 0xdf,0x2580

	htmldata 0xe0,0x03b1
	htmldata 0xe1,0x00df
	htmldata 0xe2,0x0393	; monospace font looks weird
	htmldata 0xe3,0x03c0
	htmldata 0xe4,0x03a3
	htmldata 0xe5,0x03c3
	htmldata 0xe6,0x00b5
	htmldata 0xe7,0x03c4	; monospace font looks weird
	htmldata 0xe8,0x03a6
	htmldata 0xe9,0x0398
	htmldata 0xea,0x03a9
	htmldata 0xeb,0x03b4
	htmldata 0xec,0x221e
	htmldata 0xed,0x03c6	; monospace font looks weird
	htmldata 0xee,0x03b5
	htmldata 0xef,0x2229

	htmldata 0xf0,0x2261
	htmldata 0xf1,0x00b1
	htmldata 0xf2,0x2265
	htmldata 0xf3,0x2264
	htmldata 0xf4,0x2320
	htmldata 0xf5,0x2321
	htmldata 0xf6,0x00f7
	htmldata 0xf7,0x2248
	htmldata 0xf8,0x00b0
	htmldata 0xf9,0x2219
	htmldata 0xfa,0x00b7
	htmldata 0xfb,0x221a
	htmldata 0xfc,0x207f
	htmldata 0xfd,0x00b2
	htmldata 0xfe,0x25a0
	htmldata 0xff,0x00a0	; 0x2424

HTML.TABLE_C:
	db '>&gt;',0
	db '<&lt;',0
	db '&&amp;',0
	db 0x22,'&quot;',0
	db 0x7c, '&#x254e;',0 ; 0x254f
	dw 0x0000
