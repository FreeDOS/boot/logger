; BSD 3-Clause License
; Copyright (c) 2023, Jerome Shidel

; Redistribution and use in source and binary forms, with or without
; modification, are permitted provided that the following conditions are met:

; 1. Redistributions of source code must retain the above copyright notice, this
;    list of conditions and the following disclaimer.

; 2. Redistributions in binary form must reproduce the above copyright notice,
;    this list of conditions and the following disclaimer in the documentation
;    and/or other materials provided with the distribution.

; 3. Neither the name of the copyright holder nor the names of its
;    contributors may be used to endorse or promote products derived from
;    this software without specific prior written permission.

; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
; DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
; FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
; CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

; NASM 2.15.05, or later

; -----------------------------------------------------------------------------

PrintLOG:
	; Fetch position of first character to print from the log
%ifdef AVAIL_PrepareToRead
	call		PrepareToRead
%else
	mov		al, 0x16	; Read Log
	mov		bl, 0x05	; subfunction, Get first whole line
	call far	[DRIVER_CALL]	; bypass INT 0x2d multiplexer
					; DX:CX is position data
%endif
	cmp		dx, -1		; DX=0xffff (-1) if log is empty
	jne		.Display

	; either StdOut or PrintMessage macro is fine here. However unless
	; providing ANSI or HTML output, using StdOut requires an additional
	; overhead of about 20 bytes in code.
	%ifdef ANSI_SUPPORT
		StdOut 		LogEmpty
	%elifdef HTML_SUPPORT
		StdOut 		LogEmpty
	%else
		PrintMessage 	LogEmpty
	%endif

	ret

.Display:
	mov		al, 0x16	; Read Log
	mov		bl, 0x01	; subfunction, Get Character/Color and
					; update DX for next position
	call far	[DRIVER_CALL]	; bypass INT 0x2d multiplexer
					; DX:CX is new position data or -1 (end)
					; BL is character
					; BH is color (if log is mono, BH=0x07)
	push		dx		; save high word of position data
	mov		al, bl		; set character for DOS StdOut
	StdOut		al
	pop		dx		; restore high word of position data
	cmp		dx, -1		; Are we at the end?
	jne		.Display	; if not repeat the process
	StdOutFlush
	ret
