; Boot Message Logger Interface Utility

; BSD 3-Clause License
; Copyright (c) 2023, Jerome Shidel

; Redistribution and use in source and binary forms, with or without
; modification, are permitted provided that the following conditions are met:

; 1. Redistributions of source code must retain the above copyright notice, this
;    list of conditions and the following disclaimer.

; 2. Redistributions in binary form must reproduce the above copyright notice,
;    this list of conditions and the following disclaimer in the documentation
;    and/or other materials provided with the distribution.

; 3. Neither the name of the copyright holder nor the names of its
;    contributors may be used to endorse or promote products derived from
;    this software without specific prior written permission.

; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
; DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
; FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
; CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

; NASM 2.15.05, or later

use16

cpu 8086

org 0x0100

section .text

%ifdef SINGLE_BINARY
	incbin "LOGGER.BIN"
%endif

%include 'common.inc'

%define COLOR_MESSAGE	0x0e
%define COLOR_STDIN	0x0f
%define COLOR_MONO	0x07
%define COLOR_SNAPSHOT  0x0b2d

Initialize:
	; ES & DS should already be pointed at CS.
	; push		cs
	; pop		ds
	; push		cs
	; pop		es
	; cld

	ParseOptions	OptionTable, 0x81		; cs:OptionTable
							; es:CommandLine

	and		[Flags], byte ~ofPreTest

	mov		ax, 0x3001	; Get DOS Version
	int		0x21
	cmp		al, 0x05	; Major version

	; We need at least DOS 2.0. But since some other code needs DOS 5.0, we
	; can just fail without DOS 5.0+
	jb		.DOSNoGood

.DOSOkay:

	FindDeviceDriver
	jnc		DriverFound
	xor		al, al

.DOSNoGood:

%ifdef WITH_HELP
	; test for help request
	test		[Flags], byte ofShowHelp
	jz		.NotShowHelp
	call		PrintHelp
	jmp		ExitNoError
.NotShowHelp:
%endif

	test		al, al
	jz		.NotBadDOS
	mov		dx, BadDOS
	jmp		ErrorExit

.NotBadDOS:
	mov		dx, NoDriver
	test		[Flags], byte ofBadVersion
	jz		.NotBadVersion
	mov		dx, BadDriver
.NotBadVersion:

ErrorExit:
	test		[Flags], byte ofHushMode
	jnz		.NoMessage
	PrintMessage	dx
.NoMessage:
	; Terminate with error code 1
	mov		ax,0x4c01
	int		0x21

; -----------------------------------------------------------------------------

DriverFound:

	; AH is still multiplex number, required for all INT 0x2d calls
	mov		al, 0x01		; Get Private Entry Point
	int		0x2d

	mov		[DRIVER_CALL], bx
	mov		[DRIVER_CALL+2], dx
	mov		es, dx

	mov		al, 0x11		; Set Enabled
	xor		bl, bl			; Turn Logging off
	call far	[DRIVER_CALL]
	mov		[DRIVER_ENABLED], bl	; Save previous logger state

	; test if StdIn is redirected
	mov		ax, 0x4400
	xor		bx, bx		; file handle 0 = StdIn
	int		0x21
	jc		.NoStdInput	; error
	test		dl, 0x80
	jnz		.NoStdInput	; StdIn is not redirected

	; send any standard input text to Log
	StdIn		al
	jc		.NoStdInput
	or		[Flags], byte ofStdIn + ofHadOptions

	mov		ah, COLOR_STDIN
.LoopStdIn:
	test		[Flags], byte ofPassThru
	jz		.NoPassThru
	StdOut		al
.NoPassThru:
	call		AppendBuffer
	StdIn		al
	jnc		.LoopStdIn
	StdOutFlush
	call		DriverFlush
.NoStdInput:

%ifdef WITH_HELP
	; test for help request
	test		[Flags], byte ofShowHelp
	jz		.NotShowHelp
	call		PrintHelp
	jmp		.HadOptions
.NotShowHelp:
%endif

	push		es
	push		cs
	pop		es
	ParseOptions	OptionTable, 0x81		; ds:OptionTable
							; es:CommandLine
	pop		es

	test		[Flags], byte ofHadOptions
	jnz		.HadOptions

	; if there was StdIn added to the Log, do not default to viewer when
	; no command line options are present
	test		[Flags], byte ofStdIn
	jnz		.HadOptions

	; empty command line, perform default function
	call	Option_View

.HadOptions:

	mov		al, 0x11	   ; Set Enabled
	mov		bl, [DRIVER_ENABLED] ; Restore previous enabled state
	call far	[DRIVER_CALL]

ExitNoError:
	; Terminate, no error
	mov		ax, 0x4c00
	int 		0x21

; -----------------------------------------------------------------------------

DriverFlush:
	push		ax
	mov		al, 0x12
	call		far [DRIVER_CALL]
	pop		ax
	ret

; -----------------------------------------------------------------------------

OptionTable:
	; full name options
	dw		Option_Status
	db	 	'INFORMATION', 0
	dw		Option_Off
	db		'OFF', 0
	dw		Option_On
	db	 	'ON', 0
	dw		Option_Clear
	db		'CLEAR', 0
	dw		Option_Print
	db		'PRINT', 0
	dw		Option_Msg
	db	 	'MESSAGE', 0
	dw		Option_View
	db	 	'VIEW', 0
	dw 		Option_Snapshot
	db		'SNAPSHOT', 0
	dw 		Option_PassThru
	db		'THRU', 0
	dw 		Option_Quiet
	db		'QUIET', 0

	; abbreviated options
	dw		Option_Status
	db	 	'I', 0
	dw		Option_Clear
	db		'C', 0
	dw		Option_Print
	db		'P', 0

	dw		Option_Msg
	db	 	'M', 0
	dw		Option_View
	db	 	'V', 0
	dw 		Option_Snapshot
	db		'S', 0
	dw 		Option_PassThru
	db		'T', 0
	dw 		Option_Quiet
	db		'Q', 0

%ifdef WITH_HELP
	dw		Option_Help
	db 		'HELP', 0
	dw		Option_Help
	db 		'/?', 0
	dw		Option_Help
	db 		'?', 0
%endif

%ifdef ANSI_SUPPORT
	dw		Option_ANSI
	db	 	'ANSI', 0
	dw		Option_ANSI
	db	 	'A', 0
%endif
%ifdef HTML_SUPPORT
	dw		Option_HTML
	db	 	'HTML', 0
	dw		Option_HTML
	db	 	'H', 0
%endif

	dw		0,Option_Bad ; catch all

; -----------------------------------------------------------------------------
%ifdef WITH_HELP
Option_Help:
	or		[Flags], byte ofShowHelp
	jmp		Option_Done
%endif
; -----------------------------------------------------------------------------

Option_PassThru:
	or		[Flags], byte ofPassThru ; sets high byte
	jmp		Option_Done

; -----------------------------------------------------------------------------

Option_Quiet:
	or		[Flags], byte ofHushMode ; sets high byte
	jmp		Option_Done

; -----------------------------------------------------------------------------

Option_Bad:
	PrintMessage 	BadOptionPre
	PrintOptionText
	mov		dx, BadOptionPost
	jmp		ErrorExit

Option_IgnoreRest:
	mov		al, [es:di]
	cmp		al, 0x0d
	jbe		Option_Done
	inc		di
	jmp		Option_IgnoreRest

; -----------------------------------------------------------------------------

Option_Off:
	test		[Flags], byte ofPreTest
	jnz		Option_Done
	; force off regardless of what command line options are used.
	mov		[DRIVER_ENABLED], byte 0
	jmp		Option_Done

; -----------------------------------------------------------------------------

Option_On:
	test		[Flags], byte ofPreTest
	jnz		Option_Done
	; force on regardless of what command line options are used.
	mov		[DRIVER_ENABLED], byte 1
	jmp		Option_Done

; -----------------------------------------------------------------------------

Option_Clear:
	test		[Flags], byte ofPreTest
	jnz		Option_Done
	mov		[DRIVER_ENABLED], byte 0
	mov		al, 0x13	; Clear Log
	call far	[DRIVER_CALL]
	jmp		Option_Done
; -----------------------------------------------------------------------------

Option_Print:
	test		[Flags], byte ofPreTest
	jnz		Option_Done
	mov		[DRIVER_ENABLED], byte 0
	call		PrintLOG
	jmp		Option_Done

; -----------------------------------------------------------------------------

%ifdef ANSI_SUPPORT
Option_ANSI:
	test		[Flags], byte ofPreTest
	jnz		Option_Done
	call		PrintANSI
	jmp		Option_Done
%endif

; -----------------------------------------------------------------------------

%ifdef HTML_SUPPORT
Option_HTML:
	test		[Flags], byte ofPreTest
	jnz		Option_Done
	call		PrintHTML
	jmp		Option_Done
%endif

; -----------------------------------------------------------------------------

Option_View:
	test		[Flags], byte ofPreTest
	jnz		Option_Done
	push		di

	VideoSettings
	push		bx			; save video segment for later
	push		dx			; save regen size in words
	call		ScreenSave		; only happens in supported modes

	call		LogViewer

	pop		dx			; restore regin size in words
	pop		bx			; restore video segment
	call		ScreenRestore

	pop		di
	jmp		Option_Done

; -----------------------------------------------------------------------------

Option_Snapshot:
	test		[Flags], byte ofPreTest
	jnz		Option_Done

	%include "snapshot.inc"

; -----------------------------------------------------------------------------

Option_Msg:
	test		[Flags], byte ofPreTest
	jnz		Option_IgnoreRest
	call		DriverFlush ; should already be empty, but won't hurt
	; cld
	mov		ah, COLOR_MESSAGE
	jmp		.SkipIndent
.SkipChar:
	inc		di
.SkipIndent:
	cmp		[es:di], byte 0x20
	je		.SkipChar
.NextChar:
	mov		al, [es:di]
	inc		di
	call		AppendBuffer
	cmp		al, 0x20
	jb		.AppendDone
	jmp		.NextChar
.AppendDone:
	cmp		al, 0x0d
	jne		.SkipLF
	mov		al, 0x0a
	call		AppendBuffer
.SkipLF:
	call		DriverFlush
.Done:
	jmp		Option_Done

; -----------------------------------------------------------------------------

Option_Status:
	test		[Flags], byte ofPreTest
	jnz		Option_Done
	call		PrintVersion
	PrintStatus
	jmp		Option_Done

; -----------------------------------------------------------------------------

Option_Skip:
Option_Done:
	or		[Flags], byte ofHadOptions ; there was an option flag
	ret

; -----------------------------------------------------------------------------
%ifdef WITH_HELP
PrintHelp:
	mov		dx, HelpText
	jmp		DoPrintMessage
%endif
; -----------------------------------------------------------------------------

PrintVersion:
	mov		dx, Banner

DoPrintMessage:
	PrintMessage
	ret

; -----------------------------------------------------------------------------

StringToLog:
	; ah=color
	; ds:si->String
	; cld
	push		si
.Next:
	lodsb
	cmp		al, 0x20
	jb		.Done
	cmp		al, '$'
	je		.Done
	call		AppendBuffer
	jmp		.Next
.Done:
	pop		si

	ret

; -----------------------------------------------------------------------------

AppendBuffer:
	push		ax
	push		bx
	mov		bx, ax
	mov		al, 0x14
	call		far [DRIVER_CALL]
	pop		bx
	pop		ax
	ret

; -----------------------------------------------------------------------------
%define AVAIL_PrepareToRead

PrepareToRead:
	; Fetch position of first character to print from the log
	mov		al, 0x16	; Read Log
	mov		bl, 0x05	; subfunction, Get first whole line
	call far	[DRIVER_CALL]	; bypass INT 0x2d multiplexer
					; DX:CX is position data

	; the following are used/needed only by the viewer. But, does not hurt
	; to set them when other functions (like PrintLog) are called.
	mov		ax, cx
	mov		[Viewer.Top], ax
	mov		[Viewer.Top+2], dx
	mov		[Viewer.Start], ax
	mov		[Viewer.Start+2], dx
	mov		[Viewer.Flags], byte vfAtTop + vfAtLeftMost + vfAtRightMost
	mov		[Viewer.LeftOfs], word 0
	mov		[Viewer.WidthMax], word 0
	ret

; -----------------------------------------------------------------------------

%include "viewer.inc"
%include "out_text.inc"	; simple text output

%ifdef ANSI_SUPPORT
	%include 'out_ansi.inc'
%endif

%ifdef HTML_SUPPORT
	%include 'out_html.inc'
%endif

CommonCode

; -----------------------------------------------------------------------------

section .data

AMIS_Signature

Banner:
	db	'Message Logging Interface Utility, v',VERSION,0x0d,0x0a
	CopyrightText
	db	'$'
NoDriver:
	db	'Required LOGGER driver is not loaded.',0x0d,0x0a,'$'

BadDOS:
	db	'Incompatible DOS version.',0x0d,0x0a,'$'

BadDriver:
	db 	'Incompatible LOGGER driver is loaded.',0x0d,0x0a,'$'

BadOptionPre:
	db	'Invalid option "$'
BadOptionPost:
	db	'" provided to LOGGER.',0x0d,0x0a,'$'

LogEmpty:
	db	'Log is empty.'

CRLF:
	db 	0x0d,0x0a,'$'

XMSError:
	db	'XMS error #$'

SnapshotStart:
	db	'begin snapshot$'
SnapshotEnd:
	db	'end snapshot$'

%ifdef WITH_HELP
HelpText:
	incbin  'help.inc'
	db	'$'
%endif

Flags:
	dw	ofPreTest

%ifdef HTML_SUPPORT
LastColor:
	db	0x07
%elifdef ANSI_SUPPORT
LastColor:
	db	0x07
%endif

StdIn_Data:
	dw 	0, 0			; StdIn Control Data

StdOut_Data:
	dw 	0			; StdOut Control Data

; -----------------------------------------------------------------------------

section .bss

DRIVER_CALL:		resw 1		; Offset of driver far call address
DRIVER_SEGMENT:		resw 1		; Segment of driver
DRIVER_ENABLED:		resw 1		; Driver Enabled State

StdIn_Buffer:		resb STDIO_SIZE	; used by StdIn for Appending redirected
					; text into the log
StdOut_Buffer:		resb STDIO_SIZE	; used by StdOut for PASSTHRU, PRINT &
					; ANSI. At present not used for HTML

XFR:
	.Count:		resd 1		; byte count { must be even }
	.SrcHandle:	resw 1		; XMS Handle
	.SrcAddr:	resd 1		; pointer to source buffer
	.DstHandle:	resw 1		; ; 0 = conventional memory
	.DstAddr:	resd 1		; pointer to destination

Buffer:			resw 2; Transfer Buffer

Viewer:
	.Flags:		resw 1		; Viewer navigation control flags
	.Start:		resd 1		; First Whole Log Line
	.Top:		resd 1		; Top line on screen start
	.Bottom:	resd 1		; End of Bottom line.
	.LeftOfs:	resw 1		; Start position on lines
	.WidthMax:	resw 1		; Maximum Line Width

VideoData:
	resb TVideoData_size

VideoRegen:

