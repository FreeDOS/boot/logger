; BSD 3-Clause License
; Copyright (c) 2023, Jerome Shidel

; Redistribution and use in source and binary forms, with or without
; modification, are permitted provided that the following conditions are met:

; 1. Redistributions of source code must retain the above copyright notice, this
;    list of conditions and the following disclaimer.

; 2. Redistributions in binary form must reproduce the above copyright notice,
;    this list of conditions and the following disclaimer in the documentation
;    and/or other materials provided with the distribution.

; 3. Neither the name of the copyright holder nor the names of its
;    contributors may be used to endorse or promote products derived from
;    this software without specific prior written permission.

; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
; DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
; FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
; CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

; NASM 2.15.05, or later

; -----------------------------------------------------------------------------

use16

cpu 8086

org 0x0100

section .text

; At start up, these are not required under DOS and can be assumed.
	; push 	cs
	; pop	ds	; DS = CS
	; push	cs
	; pop   es	; ES = CS
	; cld		; can be assumed

%include "common.inc"

; -----------------------------------------------------------------------------
; Command line options

	ParseOptions	OptionTable, 0x81		; cs:OptionTable
							; es:CommandLine
	jmp		Driver_Locate

OptionTable:
%ifdef WITH_HELP
	dw		Option_Help
	db 		'HELP', 0
	dw		Option_Help
	db 		'/?', 0
	dw		Option_Help
	db 		'?', 0
%endif
	dw		0,Option_Bad ; catch all

Option_Bad:
	PrintMessage	BAD_OPTION
	mov		ax, 0x4c01	; Terminate with exit code 1
	int		0x21

%ifdef WITH_HELP
Option_Help:
	PrintMessage	HELP_TEXT
	jmp		Terminate
%endif

; -----------------------------------------------------------------------------
; Check if Logger device driver is loaded and set it Multiplex ID number

Driver_Locate:
	xor		bx, bx		; initialize BH/BL to zero
	; cld				; not needed
.Scanning:
	mov		ax, bx		; set AH to next multiplex number, and
					; AL=0 for install function check

	int		0x2d		; Multiplexer

	; AMIS (Interrupt 0x2d) install check function 0, will return either
	; AL=0x00 for not in use or AL=0xFF when in use. When in use, it
	; will also return, CX=Version Number and DX:DI->Signature

	cmp		al, 0xff	; if AL=0x00, multiplex is not in use
					; if AL=0xff, multiplex is in use
	jne		.Next		; other values are an invalid response

	mov		si, LOGGER_SIG	; DS:SI->Logger signature
	mov		es, dx		; ES:DI->Returned multiplex signature
	; mov		dx, cx		; Could save version in DX for later
	mov		cx, 0x0008	; 16 bytes for comparing the signatures
	repe		cmpsw		; Compare the signatures
	je		.Found		; If matched, we found the driver
.Next:
	inc		bh		; inc BH for next multiplex to check
					; when wraps back to zero, ZF is set
					; and we tested all 256 numbers
	jnz		.Scanning	; if BH != 0, check the new multiplex

	mov		dx, NOT_FOUND	; DS:DX->Not found string
	mov		ah, 0x09	; Write string to StdOut
	int		0x21
	mov		ax, 0x4c01	; Terminate with exit code 1
	int		0x21

.Found:
	; AH & BH = Logger Multiplex ID number.
	; ES = Driver Segment

; -----------------------------------------------------------------------------
; Get far call pointer to Logger function dispatch

	; AH is still multiplex number, required for all INT 0x2d calls
	mov		al, 0x01	; Get Private Entry Point
	int		0x2d
	; if AL=0 it is not supported, AL=0xff it is supported. The current
	; Logger version and all future versions will support this function.
	; So, there is no need to check AL for support.
	; DX:BX->Point to driver's far call function dispatcher

	; The dispatcher supports all functions that are not specific to AMIS.
	; That would be all functions starting at AL=0x10 or higher.
	; They can be called through AMIS or directly through the far call to
	; the dispatcher. They provide the same return values in the same
	; registers. There could be many other programs that are hooked into
	; INT 0x2d and performance could be impacted.
	mov		[LOGGER_CALL], bx
	mov		[LOGGER_CALL+2], dx

; -----------------------------------------------------------------------------
; Flush the log buffers and Turn off logging

	; You absolutely need to flush the log buffer before writing to the log!
	; The driver uses multiple capture methods to record text and has
	; internal buffering. If you do not perform a flush, it is very probable
	; that text will be recorded out of sequence in the log or even
	; corrupted by the read process.

	; However, calling the Set Enable function ALWAYS flushes the buffers
	; so we do not need to make a special call to the Flush Log.

	; We need to turn off logging so this programs output is not written
	; back to the log. Otherwise, the log would continue to grow as we
	; displayed it's contents. Also, the read/write buffers in the driver
	; are shared and would write corrupted data into the log.

	mov		al, 0x11	; Set Logging Enabled
	xor		bl, bl		; 0, turn off
	call far	[LOGGER_CALL]	; bypass INT 0x2d multiplexer
	mov		[LOGGER_STATE], bl ; save the previous state for later

; -----------------------------------------------------------------------------

	call 		PrintANSI

; -----------------------------------------------------------------------------
; If logging was enabled, turn it back on.

RestoreState:
	mov		al, 0x11	   ; Set Enabled
	mov		bl, [LOGGER_STATE] ; Restore previous enabled state
	call far	[LOGGER_CALL]	   ; bypass INT 0x2d multiplexer

; -----------------------------------------------------------------------------
; End program

Terminate:
	mov		ax, 0x4c00	; Terminate with exit code 0
	int		0x21

; -----------------------------------------------------------------------------

%include "out_ansi.inc"

CommonCode

; -----------------------------------------------------------------------------

section .data

LOGGER_SIG:	db 'J.SHIDEL'	; 8 character manufacturer ID
		db 'LOGGERxx'	; 8 character product ID

NOT_FOUND:	db 'Logging driver not found.$'

BAD_OPTION:	db 'Invalid command line option.', 0x0d,0x0a,'$'

LogEmpty:
LOG_EMPTY:	db 'Log is empty.$'

CRLF:		dw 0x0d,0x0a,'$'

LastColor:	db 0x07

StdOut_Data:	dw 0	; StdOut Control Data

%ifdef WITH_HELP
HELP_TEXT:
	incbin "helpansi.inc"
	db "$"
%endif

; -----------------------------------------------------------------------------

section .bss

DRIVER_CALL:
LOGGER_CALL:	resd 1		; Far call to driver dispatch function
LOGGER_STATE:	resb 1		; Original Logger enabled/disabled state

StdOut_Buffer:	resb STDIO_SIZE	; Standard Output buffer