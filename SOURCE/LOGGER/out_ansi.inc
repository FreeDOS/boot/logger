; BSD 3-Clause License
; Copyright (c) 2023, Jerome Shidel

; Redistribution and use in source and binary forms, with or without
; modification, are permitted provided that the following conditions are met:

; 1. Redistributions of source code must retain the above copyright notice, this
;    list of conditions and the following disclaimer.

; 2. Redistributions in binary form must reproduce the above copyright notice,
;    this list of conditions and the following disclaimer in the documentation
;    and/or other materials provided with the distribution.

; 3. Neither the name of the copyright holder nor the names of its
;    contributors may be used to endorse or promote products derived from
;    this software without specific prior written permission.

; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
; DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
; FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
; CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

; NASM 2.15.05, or later

; -----------------------------------------------------------------------------

PrintANSI:
	; Fetch position of first character to print from the log
%ifdef AVAIL_PrepareToRead
	mov		[LastColor], byte 0x07
	call		PrepareToRead
%else
	mov		al, 0x16	; Read Log
	mov		bl, 0x05	; subfunction, Get first whole line
	call far	[DRIVER_CALL]	; bypass INT 0x2d multiplexer
					; DX:CX is position data
%endif

	cmp		dx, -1		; DX=0xffff (-1) if log is empty
	jne		.Display
	StdOut 		LogEmpty
	ret

.Display:
	mov		al, 0x16	; Read Log
	mov		bl, 0x01	; subfunction, Get Character/Color and
					; update DX for next position
	call far	[DRIVER_CALL]	; bypass INT 0x2d multiplexer
					; DX:CX is new position data or -1 (end)
					; BL is character
					; BH is color (if log is mono, BH=0x07)
	push		dx		; save high word of position data
	push		cx
	mov		dx, bx
	call		.Print
	pop		cx
	pop		dx		; restore high word of position data
	cmp		dx, -1		; Are we at the end?
	jne		.Display	; if not repeat the process
	StdOutFlush
	ret

.Print:
	cmp		dl, 0x0d
	jne		.NotCR
	mov		dh, 0x07
.NotCR:
	cmp		dl, 0x0a
	jne		.NotLF
	mov		dh, 0x07
.NotLF:

	cmp		dh, [LastColor]
	je		.ColorIsSet
	mov		[LastColor], dh

	; write ansi color change sequence
	StdOut	 	AnsiPrefix
	push		dx

	xor		bh,bh
	; blink/intensity background?
	test		dh, 0x80
	jz		.SetBackground
	mov		dl, '5'
	StdOut		dl
	mov		dl, ';'
	StdOut		dl
.SetBackground:
	mov		dl, '4'
	StdOut		dl
	mov		cl, 4
	mov		al, dh
	shr		al, cl
	and		al, 7
	mov		bl, al
	mov		dl, [AnsiColors+bx]
	StdOut		dl
	mov		dl, ';'
	StdOut		dl
	; is intensity  foreground?
	test		dh, 0x08
	jz		.SetForeground
	mov		dl, '1'
	StdOut		dl
	mov		dl, ';'
	StdOut		dl
.SetForeground:
	mov		dl, '3'
	StdOut		dl
	mov		bl, dh
	and		bl, 7
	mov		dl, [AnsiColors+bx]
	StdOut		dl
	mov		dl, 'm'
	StdOut		dl
	pop		dx
.ColorIsSet:

	cmp		dl, 0x0d
	je		.NoEscape
	cmp		dl, 0x0a
	je		.NoEscape
	cmp		dl, 0x1f
	ja		.NoEscape
	push		dx
	mov		dl, 0x1b
	StdOut		dl
	pop		dx
.NoEscape:
	StdOut		dl
	ret

AnsiPrefix:
	db	27,'[0;$'

AnsiColors:
	db	0x30,0x34,0x32,0x36,0x31,0x35,0x33,0x37
