LOG2NASI.COM [ HELP ]

	HELP, ?		Display this help text.

Output the log from LOGGER and include ANSI escape sequences to provide color.
In order to save the log to a text file, console output must be redirected.
For example:

LOG2ANSI >MYLOG.TXT
